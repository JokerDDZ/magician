﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EZCameraShake;

public class GhosCardCollision : MonoBehaviour
{

    public GameObject particleEffect;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.tag == "Enemy")
        {
            deadGhostCard();
        }
    }



    public void deadGhostCard()
    {
        Instantiate(particleEffect, transform.position, Quaternion.identity);
        CameraShaker.Instance.ShakeOnce(3f, 10f, .1f, 1f);
        Destroy(transform.parent.gameObject);
    }
}
