﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GetHitEnemy : MonoBehaviour
{
    SpriteRenderer sp;

    private void Start()
    {
        sp = GetComponent<SpriteRenderer>();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.transform.tag == "PlayerBullet")
        {
            StartCoroutine(changeColor());
        }
    }

    IEnumerator changeColor()
    {
        sp.color = new Color(1f, 0f, 0f);

        yield return new WaitForSeconds(0.2f);

        sp.color = new Color(1f, 1f, 1f);
    }
}
