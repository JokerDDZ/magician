﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletMovement : MonoBehaviour
{
    public Vector3 direction = new Vector3(1f, 0f, 0f);

    public float velocity = 6f;

    // Update is called once per frame
    void FixedUpdate()
    {
        transform.Translate(direction * velocity * Time.deltaTime);
    }
}
