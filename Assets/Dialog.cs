﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Dialog : MonoBehaviour
{
    public TextMeshProUGUI textDisplay;
    public string[] sentences;
    private int index;
    private float typingSpeed = 0.02f;
    public bool theEndOfTheDIalog = true;

    public GameObject panel;

    private void Start()
    {
        textDisplay.text = null;
        StartCoroutine(type());
    }


    private void Update()
    {
        if (Input.GetKeyDown("space") && !theEndOfTheDIalog)
        {
            if(index <= sentences.Length - 1)
            {
                if (textDisplay.text == sentences[index])
                {
                    nextSentence();
                }
            }                    
        }
    }

    IEnumerator type()
    {
        foreach (char letter in sentences[index].ToCharArray())
        {
            textDisplay.text += letter;
            yield return new WaitForSeconds(typingSpeed);
        }
    }

    public void nextSentence()
    {
        index++;

        if(index > sentences.Length - 1)
        {
            Debug.Log("Koniec dialogu");

            GameObject camera = GameObject.Find("CameraHolder");

            camera.GetComponent<CamereFollowThePlayer>().diffTarget = false;

            panel.GetComponent<Animator>().SetBool("up", false);

            Destroy(GameObject.FindGameObjectWithTag("Enemy"));

            GetComponent<EnemySpawn>().spawnEnemy = true;
            GetComponent<FirstAidKitSpawn>().spawning = true;


            GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerMovement>().movement = true;
            GameObject.FindGameObjectWithTag("Player").GetComponent<MagicSystem>().isCast = false;


            theEndOfTheDIalog = true;
        }
        else
        {
            textDisplay.text = null;
            StartCoroutine(type());
        }
    }



}
