﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TheBeginningOfDemo : MonoBehaviour
{
    public GameObject whiteSpawn;
    private GameObject player;
    public GameObject Dialog;


    private void Start()
    {       
        Instantiate(whiteSpawn, new Vector3(0f, 0f, -9f), Quaternion.identity);

        StartCoroutine(startDialog());
        StartCoroutine(findPlayer());
    }

    private IEnumerator findPlayer()
    {
        yield return new WaitForSeconds(1.02f);

        player = GameObject.FindGameObjectWithTag("Player");

        player.GetComponent<PlayerMovement>().movement = false;
        player.GetComponent<MagicSystem>().isCast = true;
        player.GetComponent<Animator>().SetBool("move", false);
    }

    private IEnumerator startDialog()
    {
        yield return new WaitForSeconds(2f);

        GameObject camera = GameObject.Find("CameraHolder");

        camera.GetComponent<CamereFollowThePlayer>().diffTarget = true;

        Dialog.GetComponent<Animator>().SetBool("up", true);
     
    }
}
