﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawn : MonoBehaviour
{
    public GameObject[] spawns = new GameObject[6];
    public List<int> spawnOrder = new List<int> {0,1,2,3,4,5};
    public GameObject portal;

    private const float timer = 5f;
    private float timeLeft = 0f;

    public bool spawnEnemy = false;

    private void Start()
    {
        createNewOrder();
    }

    // Update is called once per frame
    void Update()
    {
        timeLeft -= Time.deltaTime;

        if(timeLeft < 0 && spawnEnemy == true)
        {
            spawnPortal();

            timeLeft = timer;
        }
    }


    private void spawnPortal()
    {
        if(spawnOrder.Count == 0)
        {
            createNewOrder();
        }

        int randomSpawn = spawnOrder[0];

        spawnOrder.RemoveAt(0);

        var portalek = Instantiate(portal, spawns[randomSpawn].transform.position, Quaternion.identity);

        if(portalek.transform.position.x > 0)
        {
            portalek.GetComponent<SpriteRenderer>().flipX = true;
        }

    }


    private void refreshTheSpawnOrder()
    {
        spawnOrder = new List<int> { 0, 1, 2, 3, 4, 5 };
    }

    private void shuffleTheSpawnOrder()
    {
        for (int i = 0; i < spawnOrder.Count; i++)
        {
            int temp = spawnOrder[i];
            int randomIndex = Random.Range(i, spawnOrder.Count);
            spawnOrder[i] = spawnOrder[randomIndex];
            spawnOrder[randomIndex] = temp;
        }
    }
    
    public void createNewOrder()
    {
        refreshTheSpawnOrder();
        shuffleTheSpawnOrder();
    }
}
