﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EZCameraShake;

public class EnemyCollision : MonoBehaviour
{
    public GameObject bloodEffect;
    private float scaleOfBloodEffect = 0.9f;

    public int HP = 3;

    private void Update()
    {
        if(HP <= 0)
        {
            var krew = Instantiate(bloodEffect, transform.position - new Vector3(0f, 0.3f, -0.56f), Quaternion.identity);

            krew.transform.localScale = new Vector3(scaleOfBloodEffect, scaleOfBloodEffect, 1f);

            CameraShaker.Instance.ShakeOnce(3f,10f,.1f,1f);

            Destroy(gameObject);
        }
    }


    private void OnCollisionEnter2D(Collision2D collision)
    {

        if (collision.transform.tag == "PlayerBullet")
        {
            HP -= 1;
            Destroy(collision.gameObject);
        }
        else if(collision.transform.tag == "PlayerWybuch")
        {
            HP -= 2;
        }
    }

}
