﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyObjectAfterTime : MonoBehaviour
{

    public float timer = 3f;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(destroyObject());
    }

    IEnumerator destroyObject()
    {
        yield return new WaitForSeconds(timer);

        Destroy(gameObject);
    }
}
