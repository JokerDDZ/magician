﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamereFollowThePlayer : MonoBehaviour
{

    private GameObject target;
    private float  smoothSpeed = 0.3f;
    private Vector3 offset = new Vector3(0f, 0f, -10f);
    public bool diffTarget = false;


    // Update is called once per frame
    void FixedUpdate()
    {

        if(!diffTarget)
        {
            target = GameObject.FindGameObjectWithTag("Player");
        }
        else
        {
            target = GameObject.FindGameObjectWithTag("CameraPlace");
        }
        

        if(target != null)
        {
            Vector2 myPosition = new Vector2(transform.position.x, transform.position.y);
            Vector2 playerPosition = new Vector2(target.transform.position.x, target.transform.position.y);

            Vector3 smoothPostion = transform.position = Vector2.Lerp(myPosition, playerPosition, smoothSpeed);

            transform.position = smoothPostion + offset;
        }
             
    }
}
