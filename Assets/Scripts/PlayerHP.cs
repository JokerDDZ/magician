﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHP : MonoBehaviour
{
    public float HP;
    private float maxHP = 100;

    private bool once = false;

    private GameObject healthBar;
    private Animator am;

    private void Start()
    {
        am = GetComponent<Animator>();

        HP = maxHP;

        healthBar = GameObject.Find("HealthBar");
    }

    void Update()
    {
        healthBar.GetComponent<Image>().fillAmount = HP / maxHP;

        if(HP > maxHP)
        {
            HP = maxHP;
        }

        if(HP <= 0)
        {
            if(!once)
            {
               am.SetBool("die", true);
                once = true;
            }
            
        }
    }

    public void blockDie()
    {
      am.SetBool("die", false);
    }
    
    public void deactivatePlayer()
    {
        GetComponent<Rigidbody2D>().velocity = new Vector3(0f, 0f, 0f);

        MonoBehaviour[] scripts = gameObject.GetComponents<MonoBehaviour>();
        
       am.SetBool("SpellFourth",false);

        foreach (MonoBehaviour script in scripts)
        {
            script.enabled = false;
        }

        GetComponent<BoxCollider2D>().enabled = false;
    }
}
