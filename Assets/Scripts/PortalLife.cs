﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PortalLife : MonoBehaviour
{

    public GameObject zombie;
    public GameObject ghost;


    private Vector3 offset = new Vector3(0, -0.4f, 0);
    private const float timer = 2f;

    public void startSpawning()
    {
        StartCoroutine(spawnEnemy());
    }


    IEnumerator spawnEnemy()
    {
        yield return new WaitForSeconds(timer);

        int randomEnemy = Random.Range(0, 2);


        Debug.Log(randomEnemy);
        //Debug
        //randomEnemy = 1;

        if(randomEnemy == 0)
        {
            Instantiate(zombie, transform.position + offset, Quaternion.identity);
        }
        else if(randomEnemy == 1)
        {
            Instantiate(ghost, transform.position + offset, Quaternion.identity);
        }

        

        yield return new WaitForSeconds(timer);

        Destroy(gameObject);
    }
}
