﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlipOfEnemy : MonoBehaviour
{

    private GameObject player;

    private Vector3 destination;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
    }

    // Update is called once per frame
    void Update()
    {
        if(player != null)
        {
            destination = player.transform.position - transform.position;

            if (destination.x > 0)
            {
                GetComponent<SpriteRenderer>().flipX = true;
            }
            else
            {
                GetComponent<SpriteRenderer>().flipX = false;
            }
        }
        
    }
}
