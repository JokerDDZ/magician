﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    float horizontalMove = 0f;
    private Vector2 movementDir;
    private float velocity = 3f;

    public bool movement = true;

    private Rigidbody2D rb;

    private const float timer = 0.5f;
    private float timeLeft;

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        if (movement)
        {
            movementDir = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));



            if (Input.GetAxisRaw("Horizontal") != 0 || Input.GetAxisRaw("Vertical") != 0)
            {
                GetComponent<Animator>().SetBool("move", true);
            }
            else
            {
                GetComponent<Animator>().SetBool("move", false);
            }


            if (movementDir.x < 0)
            {
                GetComponent<SpriteRenderer>().flipX = true;
            }
            else if (movementDir.x > 0)
            {
                GetComponent<SpriteRenderer>().flipX = false;
            }
        }
        else
        {
            movementDir = new Vector3(0f, 0f, 0f);
        }
    }


    private void FixedUpdate()
    {
        transform.Translate(movementDir * velocity * Time.deltaTime);         
    }

}
