﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MagicSystem : MonoBehaviour
{
    private GameObject[] magia = new GameObject[3];
    private SpriteRenderer sp;

    public GameObject bulletSpell;
    public GameObject portalPlayer;
    public GameObject kapeluszSpell;
    public GameObject cardSpell;

    private const float timer = 0.8f;
    private float timeLeft = 0f;

    public bool isCast = false;

    private Vector3 offsetSpellPierwszy = new Vector3(-0.3f, 0f, 0f);
    private Vector3 offsetSpellPierwszyWysokosc = new Vector3(0f, 0.2f, 0f);


    private void Start()
    {
        sp = GetComponent<SpriteRenderer>();

        magia[0] = GameObject.Find("FirstPole");
        magia[1] = GameObject.Find("SecondPole");
        magia[2] = GameObject.Find("ThirdPole");
    }

    // Update is called once per frame
    void Update()
    {
        //////////////////////timer
        timeLeft -= Time.deltaTime;
           

        if (timeLeft < 0)
        {
            czyszczenieMagii();

            timeLeft = timer;
        }

        /////////////////////////

        if(!isCast)
        {
            if (Input.GetKeyDown("j"))
            {
                colorOfMagic(new Color(1f, 0f, 0f));
            }
            else if(Input.GetKeyDown("k"))
            {
                colorOfMagic(new Color(0f, 0f, 1f));
            }
        }
        
        whatSpellCast();
    }

    private void przesuniecieMagii()
    {
        magia[2].GetComponent<Image>().color = magia[1].GetComponent<Image>().color;
        magia[1].GetComponent<Image>().color = magia[0].GetComponent<Image>().color;
    }

    private void czyszczenieMagii()
    {    
            for (int i = 0; i < 3; i++)
            {
                magia[i].GetComponent<Image>().color = new Color(1f, 1f, 1f);
            }     
    }

    private void whatSpellCast()
    {
        int howMuchRed = 0;
        int howMuchBlue = 0;

        for (int i = 0; i < 3; i++)
        {
            if (magia[i].GetComponent<Image>().color == new Color(1f, 0f, 0f))
            {
                howMuchRed += 1;
            }
            else if (magia[i].GetComponent<Image>().color == new Color(0f, 0f, 1f))
            {
                howMuchBlue += 1;
            }
        }

        if(howMuchRed == 3)
        {
                czyszczenieMagii();

                GetComponent<Animator>().SetBool("SpellFirst", true);

                GetComponent<PlayerHP>().HP -= 20;
        }
        else if(howMuchBlue == 3)
        {
                czyszczenieMagii();

                GetComponent<Animator>().SetBool("SpellSecond", true);

                GetComponent<PlayerHP>().HP += 5;
        }
        else if(howMuchBlue == 1 && howMuchRed == 2)
        {
                czyszczenieMagii();

                GetComponent<Animator>().SetBool("SpellThird", true);

                GetComponent<PlayerHP>().HP -= 10;
        }
        else if(howMuchBlue == 2 && howMuchRed == 1)
        {
                czyszczenieMagii();

                GetComponent<Animator>().SetBool("SpellFourth", true);

                GetComponent<PlayerHP>().HP -= 30;
        }     
    }

    private void utwórzKuleKrwi()
    {
        if (sp.flipX == true)
        {
            var kuleczka = Instantiate(bulletSpell, transform.transform.position + offsetSpellPierwszy + offsetSpellPierwszyWysokosc, Quaternion.identity);
            kuleczka.GetComponent<PlayerBulletMovement>().direction *= -1f;
            sp.flipX = true;
            kuleczka.transform.GetChild(0).transform.Rotate(0f, 0f, 180f);
        }
        else
        {
            Instantiate(bulletSpell, transform.transform.position - offsetSpellPierwszy + offsetSpellPierwszyWysokosc, Quaternion.identity);
        }
    }


    public void freezeMovement()
    {
        GetComponent<PlayerMovement>().movement = false;
        GetComponent<Animator>().SetBool("SpellFirst", false);
        GetComponent<Animator>().SetBool("SpellSecond", false);
        GetComponent<Animator>().SetBool("SpellThird", false);
        GetComponent<Animator>().SetBool("SpellFourth", false);
        isCast = true;
    }

    public void castSpell()
    {
        utwórzKuleKrwi();
    }

    public void castKapeluszSpell()
    {
        Vector3 distanceFromPlayer = new Vector3(0.5f, 0f, 0f);
        Vector3 distanceBetweenHats = new Vector3(0f, 0.5f, 0f);
        Vector3 yOffset = new Vector3(0f, -0.5f, 0f);

        if(sp.flipX == true)
        {
            distanceFromPlayer = -distanceFromPlayer;
        }

        for(int i = 0; i < 3; i++)
        {
            Instantiate(kapeluszSpell, transform.position + distanceFromPlayer + distanceBetweenHats * i + yOffset, Quaternion.identity);
        }
    }



    public void castCardSpell()
    {
        GameObject[] ghosts = GameObject.FindGameObjectsWithTag("GhostCard");

        foreach(GameObject ghost in ghosts)
        {
             ghost.transform.GetChild(0).GetComponent<GhosCardCollision>().deadGhostCard();
        }
       
        if(sp.flipX == false)
        {
            Vector3 distanceFromPlayer = new Vector3(0.5f, 0f, 0f);

            Instantiate(cardSpell, transform.position + distanceFromPlayer, Quaternion.identity);
        }
        else
        {
            Vector3 distanceFromPlayer = new Vector3(0.5f, 0f, 0f);

            Instantiate(cardSpell, transform.position - distanceFromPlayer, Quaternion.identity);
        }
        
    }
    
    public void resumeMove()
    {
        GetComponent<PlayerMovement>().movement = true;
        isCast = false;
    }

    private void colorOfMagic(Color color)
    {
        przesuniecieMagii();
        magia[0].GetComponent<Image>().color = color;
        timeLeft = timer;
    }

    public void createPortal()
    {
        Instantiate(portalPlayer, transform.position + new Vector3(0f,-0.1f,0f), Quaternion.identity);
    }


    public void portalChangePosition()
    {
        float destination = 3f;

        if (sp.flipX == true)
        {
            transform.position -= new Vector3(destination, 0f, 0f);
        }
        else
        {
            transform.position += new Vector3(destination, 0f, 0f);
        }
    }
}
