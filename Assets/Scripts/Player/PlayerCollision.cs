﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCollision : MonoBehaviour
{
    private const float timer = 0.2f;
    private float timeLeft = 0f;
    private bool push = false;
    private const float speed = 700f;
    private bool isMovementIsBack = false;

    private Vector2 directionToPush;
    private Rigidbody2D rb;
    private SpriteRenderer sp;


    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        sp = GetComponent<SpriteRenderer>();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.transform.tag == "Enemy")
        {
            GetComponent<PlayerHP>().HP -= 10;

            if(collision.transform.position.x > transform.position.x)
            {
                //do tylu
                directionToPush = new Vector2(-1f, 0f);
                timeLeft = timer;


                GetComponent<Rigidbody2D>().AddForce(directionToPush * speed);

                StartCoroutine(immortality());
            }
            else
            {
                //do przodu

                directionToPush = new Vector2(1f,0f);
                timeLeft = timer;

                GetComponent<SpriteRenderer>().flipX = true;
                GetComponent<Rigidbody2D>().AddForce(directionToPush * speed);

                StartCoroutine(immortality());
            }
        }
    }

    private void Update()
    {
        timeLeft -= Time.deltaTime;

        if(timeLeft > 0)
        {       
            GetComponent<PlayerMovement>().movement = false;
            GetComponent<MagicSystem>().isCast = true;
            isMovementIsBack = false;
            GetComponent<Animator>().SetBool("move", false);
        }
        else
        {
            if(!isMovementIsBack)
            {
                GetComponent<PlayerMovement>().movement = true;
                GetComponent<MagicSystem>().isCast = false;
                isMovementIsBack = true;
            }

            rb.velocity = new Vector3(0f, 0f, 0f);
        }
    }

    IEnumerator immortality()
    {
        GetComponent<BoxCollider2D>().enabled = false;

        for(int i = 0; i < 10;i++)
        {
            yield return new WaitForSeconds(0.1f);

           if(sp.enabled == true)
           {
                sp.enabled = false;
           }
           else
           {
                sp.enabled = true;
           }
        }

        sp.enabled = true;
        GetComponent<BoxCollider2D>().enabled = true;
    }

}
