﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GhostParticlesEffect : MonoBehaviour
{

    public GameObject particleEffect;

    private float offset = 0.4f;


    private void Start()
    {
       var karty =  Instantiate(particleEffect, transform.position - new Vector3(0f, offset, 0f), Quaternion.identity);

        karty.transform.parent = gameObject.transform;
    }

}
