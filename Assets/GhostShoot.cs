﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GhostShoot : MonoBehaviour
{
    public GameObject bullet;

    private const float timer = 2f;
    private float timeLeft = 0f;

    private void Start()
    {
        timeLeft = timer;
    }

    private void Update()
    {
        timeLeft -= Time.deltaTime;

        if(timeLeft < 0 )
        {
            GameObject []  enemies = GameObject.FindGameObjectsWithTag("Enemy");

            if(enemies.Length != 0)
            {
                Instantiate(bullet, transform.position, Quaternion.identity);
            }


            timeLeft = timer;
        }
    }
}
