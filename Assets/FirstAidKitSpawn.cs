﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstAidKitSpawn : MonoBehaviour
{
    private const float timer = 5f;
    private float timeLeft;

    public GameObject firstAidKit;
    public bool spawning = false;


    // Start is called before the first frame update
    void Start()
    {
        timeLeft = timer;
    }

    private void Update()
    {
        if(spawning)
        {
            timeLeft -= Time.deltaTime;

            if (timeLeft < 0)
            {
                float x = Random.Range(-9.4f, 9.4f);
                float y = Random.Range(-4.8f, 4.8f);

                Instantiate(firstAidKit, new Vector3(x, y, -9f), Quaternion.identity);

                timeLeft = timer;
            }
        }
       
    }
}
