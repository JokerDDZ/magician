﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PortalPlayerLifeTime : MonoBehaviour
{
    public GameObject particles;

    public void destroyPortal()
    {
        Destroy(gameObject);
    }

    public void particlesPortal()
    {
        Instantiate(particles, transform.position - new Vector3(0f,0.45f,0f), Quaternion.identity);
    }
}
