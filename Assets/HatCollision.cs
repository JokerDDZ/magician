﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HatCollision : MonoBehaviour
{
    public GameObject wybuch;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.transform.tag == "Enemy")
        {
            Instantiate(wybuch, transform.position, Quaternion.identity);
            Destroy(gameObject);
        }
    }
}
