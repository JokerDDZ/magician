﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticlesForLittleBlood : MonoBehaviour
{
    public GameObject particles;

    private void OnDestroy()
    {
        Instantiate(particles, transform.position, Quaternion.identity);
    }
}
