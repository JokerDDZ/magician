﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LifeTimeOfSpawner : MonoBehaviour
{

    public GameObject player;

    public void spawnPlayer()
    {
        Instantiate(player, new Vector3(0f, 0.1f, -9f), Quaternion.identity);
    }

    public void destroyYourself()
    {
        Destroy(gameObject);
    }
}
