﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GhostAttack : MonoBehaviour
{
    public GameObject bullet;

    private SpriteRenderer sp;
    private GameObject player;
    private bool readyToShoot = true;


    private void Start()
    {
        sp = GetComponent<SpriteRenderer>();
    }

    public void tryToAttack()
    {
        player = GameObject.FindGameObjectWithTag("Player");

        if(player == null)
        {
            return;
        }

        var distanceInX = Mathf.Abs(player.transform.position.x - transform.position.x);
        var distanceInY = Mathf.Abs(player.transform.position.y - transform.position.y);


        if(distanceInX < 4 && distanceInY < 3 && readyToShoot == true)
        {

            readyToShoot = false;
            GetComponent<Animator>().SetBool("attack", true);
            StartCoroutine(cdOnShoot());
        }
    }


    public void createThreeBullets()
    {
        Vector3 distanceFromGhost = new Vector3(0.3f, 0f, 0f);

        if(sp.flipX == true)
        {
            var b = Instantiate(bullet, transform.position + distanceFromGhost, Quaternion.identity);
            b.GetComponent<BulletMovement>().direction = new Vector3(1, 0f, 0f);
            b.GetComponent<SpriteRenderer>().flipX = true;

            b = Instantiate(bullet, transform.position + distanceFromGhost, Quaternion.identity);
            b.GetComponent<BulletMovement>().direction = new Vector3(1, -1f, 0f);
            b.GetComponent<SpriteRenderer>().flipX = true;

            b = Instantiate(bullet, transform.position + distanceFromGhost, Quaternion.identity);
            b.GetComponent<BulletMovement>().direction = new Vector3(1, 1f, 0f);
            b.GetComponent<SpriteRenderer>().flipX = true;


        }
        else
        {
            var b = Instantiate(bullet, transform.position - distanceFromGhost, Quaternion.identity);
            b.GetComponent<BulletMovement>().direction = new Vector3(-1, 0f, 0f);

            b = Instantiate(bullet, transform.position - distanceFromGhost, Quaternion.identity);
            b.GetComponent<BulletMovement>().direction = new Vector3(-1, -1f, 0f);

            b = Instantiate(bullet, transform.position - distanceFromGhost, Quaternion.identity);
            b.GetComponent<BulletMovement>().direction = new Vector3(-1, 1f, 0f);
        }
        
    }

    public void goMovement()
    {
        GetComponent<Animator>().SetBool("attack", false);
    }

    private IEnumerator cdOnShoot()
    {
        yield return new WaitForSeconds(3f);

        readyToShoot = true;
    }
}
