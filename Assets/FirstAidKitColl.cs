﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstAidKitColl : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Player")
        {
            collision.GetComponent<PlayerHP>().HP += 30;

            Destroy(gameObject);
        }
    }
}
