﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BloodAutoPilotMovement : MonoBehaviour
{
    GameObject bullet;
    Rigidbody2D bulletRigidbody;
    public Transform target;
    public float rotSpeed = 500;
    public float range = 0f;
    float bulletSpeed = 3f;

    public void Start()
    {
        bullet = gameObject;
        bulletRigidbody = bullet.GetComponent<Rigidbody2D>();

        InvokeRepeating("updateTarget", 0f, 0.2f);
    }

    private void FixedUpdate()
    {
        bulletRigidbody.velocity = bullet.transform.up * bulletSpeed;

        if (target != null)
        {
            Vector3 dir = target.transform.position - transform.position;
            dir.Normalize();


            float zAngle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;


            Quaternion desireRot = Quaternion.Euler(0, 0, zAngle -90);

            transform.rotation = Quaternion.RotateTowards(transform.rotation, desireRot, rotSpeed * Time.deltaTime);
        }
    }

    void updateTarget()
    {
        var enemies = GameObject.FindGameObjectsWithTag("Enemy");


        float shortestDistance = Mathf.Infinity;
        GameObject nearestEnemy = null;


        
        foreach (GameObject enemy in enemies)
        {

            float distanceToEnemy = Vector3.Distance(transform.position, enemy.transform.position);

           
            if (shortestDistance > distanceToEnemy)
            {
                shortestDistance = distanceToEnemy;
                nearestEnemy = enemy;
            }
            
        }

        if (target == null && shortestDistance <= range)
        {
            target = nearestEnemy.transform;
        }
    }

    public void setSpeed(float speed)
    {
        this.bulletSpeed = speed;
    }

}
