﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GhostBulletGoRight : MonoBehaviour
{

    private float speed = 7f;

    private void Start()
    {
        GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");

        GameObject target = null;

        for(int i = 0; i < enemies.Length;i++)
        {
            if(i == 0)
            {
                target = enemies[i];
            }

            if(Vector3.Distance(transform.position,target.transform.position) > Vector3.Distance(transform.position, enemies[i].transform.position))
            {
                target = enemies[i];
            }

        }

        //transform.LookAt(target.transform);

        var dir = target.transform.position - transform.position;
        var angle = (Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg) - 90f;
        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);

    }


    // Update is called once per frame
    void FixedUpdate()
    {
        transform.Translate(Vector3.up * Time.deltaTime * speed);
    }    

}
