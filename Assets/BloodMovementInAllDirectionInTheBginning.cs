﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BloodMovementInAllDirectionInTheBginning : MonoBehaviour
{
    Rigidbody2D rb;

    private float velocity = 60f;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        goInRandomDirection();
        StartCoroutine(startAutoPilot());
    }

    IEnumerator startAutoPilot()
    {
        yield return new WaitForSeconds(2f);

        rb.velocity = new Vector2(0f, 0f);
        GetComponent<CircleCollider2D>().enabled = true;
        GetComponent<BloodAutoPilotMovement>().enabled = true;
    }

    void goInRandomDirection()
    {
        float randomDirection = Random.Range(0, 360);

        transform.rotation = Quaternion.Euler(0, 0, randomDirection);

        rb.AddForce(transform.up * velocity);
    }
}
